# URL to MP3

Take a url of a song or playlist and receive all the mp3 file(s)

## Getting Started

Please see Downloads section of BitBucket and download the playlistdownloader.zip file


### Installing

Google Chrome is required for the program to function


## How to Use:

You must download your playlist as an .htm file with Google Chrome

Go to Spotify and open Playlist of choice. Hit ’Share’ and copy link. 

Open Chrome, paste the link, and hit command-s (ctrl-s Windows) and in the drop-down menu, Save as Webpage, HTML only.

For Mac users:

- Locate the html file (usually in Downloads)
- Right click (two finger click) file and select Get Info
![Get Info](images/getinfo.png)
- Under General > Where: select the file path (NOTE: The file path does NOT include the actual name of the file. See Name & Extension for the name of the file)
![File Path](images/filepath.png)
- Go into the zip file with the program and unzip it wherever you please (double click zip file to unzip in Downloads folder)
- Double click playlistdownloader executable file (if that does not work right click and hit open) 
![playlistdownloader](images/playlistex.png)
- Where the program asks for the file path in the Terminal window, paste the file path. Be sure to add the file name to the end of the path. 

```
Please enter the file path (Example: /Users/Alex/Downloads/Testing.htm): /Users/Alex/Downloads/Testing.htm
```
From there, the program will open Chrome windows on its own and run automatically. The song downloads will be found in your chrome default download folder.



## Author

* **Alexander Faus**