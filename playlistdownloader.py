from splinter import Browser
import selenium
from selenium import webdriver
from selenium.webdriver import Chrome
from splinter.driver.webdriver.chrome import WebDriver as ChromeWebDriver
from bs4 import BeautifulSoup
import time


def single_song(link):
    browser = Browser('chrome')
    browser.visit(link)
    song_name = browser.title
    song_name_list = song_name.split('-')
    song_title = song_name_list[1]
    song_artist = song_name_list[2]
    search_term = song_title + song_artist + " audio"
    search_term = search_term.replace(" ", "+")
    search_term = search_term.replace("&", "%26")
    search_term = search_term.replace("!", "%21")
    search_term = search_term.replace("#", "%23")
    search_term = search_term.replace("$", "%24")
    search_term = search_term.replace("%", "%25")
    search_term = search_term.replace("\'", "%27")
    search_term = search_term.replace("(", "%28")
    search_term = search_term.replace(")", "%29")
    search_term = search_term.replace("*", "%2A")
    search_term = search_term.replace(",", "%2C")
    search_term = search_term.replace(".", "%2E")
    link = "https://www.youtube.com/results?search_query=" + search_term
    browser.visit(link)
    html = browser.html
    soup = BeautifulSoup(html, "html.parser")
    vidlist = []
    for vid in soup.findAll(attrs={'class':'yt-uix-tile-link'}):
        vidlist.append('https://www.youtube.com' + vid['href'])
    download_link = vidlist[0]
    browser.visit('http://www.theyoump3.com/')
    browser.fill('url', download_link)
    button = browser.find_by_xpath('/html/body/div[1]/div/div[1]/div[2]/div/div[1]/form/div/span/button')
    button.click()
    browser.windows[0].close_others()
    time.sleep(5)
    download = browser.find_by_id('downloadLink')
    download.click()
    yes = True
    while(yes):
        if browser.title == 'Successful - TheYouMp3':
            yes = False
        time.sleep(1)
    browser.quit()


def playlist(filename):
    song_list = []
    html = file(filename).read()
    soup = BeautifulSoup(html, "html.parser")
    for tag in soup.find_all("meta", property="music:song"):
        song_list.append(tag["content"])
    for song in song_list:
        single_song(song)


def main():
    print("\nWelcome to the Spotify playlist url to mp3 downloader" + "\nPlease make sure to "
                                                                    "carefully read through the overview page: "
                                                                    "https://bitbucket.org/alexfaus/musicwebapp/overview")
    filename = raw_input("\n\nPlease enter the file path "
                         "(Example: /Users/Alex/Downloads/Testing.htm): ")
    # show an "Open" dialog box and return the path to the selected file
    playlist(filename)

main()